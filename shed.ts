interface ShedNodeParams {
  classes?: string[];
  id?: string;
  style?: string;
  text?: string;
  html?: string;
  type?: string;
  placeholder?: string;
  href?: string;
  target?: string;
  title?: string;
  value?: string;
  oninit?(node);
  onkeypress?(node, event);
  onclick?(node, event);
  oninput?(node, event);
  onchange?(node, event);
}


class ShedMount {

  protected currentNode: ShedNode = null;
  protected baseElement;

  constructor(protected element,
              protected view) {
    this.element.innerHTML = '';
  }

  /**
   * Create view or apply differences.
   */
  render = () => {
    let newNode = this.view();
    if (!this.currentNode) {
      this.baseElement = newNode.getHtml();
      this.element.appendChild(this.baseElement);
    }
    else {
      newNode.diff(this.baseElement, this.currentNode);
    }
    this.currentNode = newNode;
  };

}


class ShedNode {
  tag: string = '';
  params: ShedNodeParams = {};
  entries: ShedNode[] = [];

  getHtml = () => {
    let element = document.createElement(this.tag);
    if (this.entries.length > 0) {
      this.entries.forEach((entry: ShedNode) => {
        element.appendChild(entry.getHtml());
      });
    }
    if (this.params) {
      this.setParamsToElement(element);
    }
    return element;
  };

  diff = (element, prevNode: ShedNode) => {
    // diff entries
    if (this.entries.length !== prevNode.entries.length) {
      element.innerHTML = '';
      this.entries.forEach((entry: ShedNode) => {
        element.appendChild(entry.getHtml());
      });
    }
    else {
      this.entries.forEach((entry: ShedNode, index: number) => {
        entry.diff(element.childNodes[index], prevNode.entries[index]);
      });
    }
    if (this.params) {
      this.setParamsToElement(element);
    }
  };

  protected setParamsToElement = (element) => {
    if (this.params.hasOwnProperty('classes') && this.params.classes.length > 0) {
      this.setChangedAttribute(element, 'class', this.params.classes.join(' '));
    }
    if (this.params.hasOwnProperty('id')) {
      this.setChangedAttribute(element, 'id', this.params.id);
    }
    if (this.params.hasOwnProperty('style')) {
      this.setChangedAttribute(element, 'style', this.params.style);
    }
    if (this.params.hasOwnProperty('text')) {
      if (element.innerText != this.params.text) {
        element.innerText = this.params.text;
      }
    }
    if (this.params.hasOwnProperty('html')) {
      if (element.innerHTML != this.params.html) {
        element.innerHTML = this.params.html;
      }
    }
    if (this.params.hasOwnProperty('type')) {
      this.setChangedAttribute(element, 'type', this.params.type);
    }
    if (this.params.hasOwnProperty('placeholder')) {
      this.setChangedAttribute(element, 'placeholder', this.params.placeholder);
    }
    if (this.params.hasOwnProperty('href')) {
      this.setChangedAttribute(element, 'href', this.params.href);
    }
    if (this.params.hasOwnProperty('target')) {
      this.setChangedAttribute(element, 'target', this.params.target);
    }
    if (this.params.hasOwnProperty('title')) {
      this.setChangedAttribute(element, 'title', this.params.title);
    }
    if (this.params.hasOwnProperty('value')) {
      element.value = this.params.value;
    }
    // events
    if (this.params.hasOwnProperty('onkeypress')) {
      element.onkeypress = (e) => this.params.onkeypress(element, e);
    }
    if (this.params.hasOwnProperty('onclick')) {
      element.onclick = (e) => this.params.onclick(element, e);
    }
    if (this.params.hasOwnProperty('oninput')) {
      element.oninput = (e) => this.params.oninput(element, e);
    }
    if (this.params.hasOwnProperty('onchange')) {
      element.onchange = (e) => this.params.onchange(element, e);
    }
    // init
    if (this.params.hasOwnProperty('oninit')) {
      this.params.oninit(element);
    }
  };

  protected setChangedAttribute = (element, name, newValue) => {
    if (element.getAttribute(name) != newValue) {
      element.setAttribute(name, newValue);
    }
  }
}


function n(tag, params: ShedNodeParams = {}, entries: ShedNode[] = []): ShedNode {
  let node = new ShedNode();

  let tagParts = tag.split('.');
  if (tagParts[1]) {
    if (params.classes) {
      params.classes.push(tagParts[1]);
    }
    else {
      params.classes = [tagParts[1]];
    }
  }

  node.tag = tagParts[0];
  node.params = params;
  node.entries = entries.filter(entry => !!entry);

  if (params.text && node.entries.length > 0) {
    throw new Error('Text param is allowed only without entries!');
  }

  return node;
}